.. -*- fooooo: fooooo -*-

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
fooooo’fooooo fooooo fooooo fooooo fooooo fooooo
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. fooooo::

fooooo fooooo
==================

fooooo fooooo fooooo fooooo fooooo fooooo fooooo `fooooo
<fooooo://fooooo.fooooo.fooooo/fooooo/fooooo%fooooo%fooooo>` fooooo.
fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo:

fooooo fooooo fooooo fooooo fooooo fooooo-fooooo fooooo fooooo
  fooooo fooooo fooooo fooooo fooooo fooooo fooooo-fooooo fooooo fooooo,
  fooooo fooooo fooooo fooooo fooooo fooooo, fooooo fooooo fooooo fooooo fooooo
  fooooo fooooo fooooo fooooo fooooo.  fooooo fooooo fooooo fooooo fooooo fooooo fooooo
  fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo.

fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo
  fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo
  fooooo fooooo fooooo fooooo fooooo'fooooo fooooo fooooo fooooo fooooo-fooooo “fooooo”
  fooooo.

  fooooo fooooo fooooo, fooooo fooooo fooooo fooooo fooooo fooooo fooooo:

  fooooo.  fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo?
  fooooo.  fooooo fooooo fooooo fooooo fooooo fooooo fooooo?
  fooooo.  fooooo fooooo fooooo?

  fooooo fooooo fooooo fooooo fooooo, fooooo fooooo fooooo fooooo fooooo fooooo fooooo
  fooooo fooooo fooooo fooooo fooooo, fooooo fooooo fooooo fooooo fooooo fooooo
  fooooo fooooo fooooo fooooo fooooo, fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo
  fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo.

fooooo fooooo fooooo
  fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo
  `fooooo fooooo <fooooo://fooooo.fooooo.fooooo/fooooo/>` fooooo.  fooooo
  fooooo-fooooo fooooo fooooo fooooo fooooo fooooo “fooooo fooooo”
  fooooo fooooo fooooo fooooo fooooo fooooo fooooo “fooooo”.  fooooo fooooo
  fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo
  fooooo.  fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo
  fooooo fooooo fooooo fooooo.

  fooooo fooooo fooooo fooooo fooooo fooooo fooooo `fooooo fooooo
  <fooooo://fooooo.fooooo.fooooo/fooooo/fooooo/fooooo/fooooo/fooooo>` fooooo.

fooooo fooooo fooooo fooooo fooooo fooooo
  fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo.  fooooo fooooo
  fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo
  fooooo fooooo.

fooooo fooooo fooooo fooooo fooooo
  fooooo fooooo fooooo fooooo fooooo fooooo, fooooo fooooo fooooo fooooo fooooo fooooo fooooo
  fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo.
  fooooo fooooo fooooo fooooo fooooo.

  fooooo fooooo fooooo fooooo, fooooo fooooo fooooo fooooo fooooo (fooooo,
  fooooo, fooooo fooooo, fooooo fooooo fooooo fooooo fooooo fooooo
  fooooo) fooooo fooooo fooooo fooooo fooooo.

fooooo fooooo fooooo
  fooooo fooooo fooooo fooooo fooooo fooooo:

  * fooooo fooooo, fooooo fooooo fooooo.fooooo fooooo fooooo.fooooo, fooooo fooooo fooooo fooooo.
  * fooooo fooooo, fooooo fooooo fooooo.fooooo.fooooo fooooo fooooo.fooooo.fooooo, fooooo fooooo–fooooo fooooo fooooo fooooo.
  * fooooo fooooo, fooooo fooooo fooooo.fooooo.fooooo.fooooo fooooo fooooo.fooooo.fooooo.fooooo, fooooo fooooo fooooo fooooo
    fooooo fooooo.  fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo
    fooooo fooooo.
  * fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo `fooooo fooooo
    <fooooo://fooooo.fooooo.fooooo/fooooo/fooooo/fooooo/fooooo/fooooo>` fooooo
    fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo.

  fooooo fooooo fooooo fooooo fooooo fooooo, fooooo fooooo fooooo fooooo fooooo fooooo;
  fooooo fooooo fooooo fooooo, fooooo fooooo fooooo fooooo fooooo fooooo fooooo
  fooooo fooooo fooooo fooooo fooooo fooooo.  fooooo fooooo fooooo fooooo
  fooooo fooooo:

  * fooooo fooooo, fooooo fooooo fooooo.fooooo.fooooo, fooooo.fooooo.fooooo, fooooo.fooooo.fooooo, fooooo fooooo–fooooo
    fooooo fooooo fooooo.
  * fooooo fooooo, fooooo fooooo fooooo.fooooo.fooooo, fooooo.fooooo.fooooo, fooooo fooooo fooooo
    fooooo fooooo fooooo.  fooooo fooooo fooooo fooooo fooooo
    fooooo fooooo fooooo fooooo fooooo.
  * fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo `fooooo fooooo
    <fooooo://fooooo.fooooo.fooooo/fooooo/fooooo/fooooo/fooooo/fooooo>` fooooo
    fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo.

  fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo, fooooo fooooo fooooo fooooo fooooo,
  fooooo fooooo fooooo fooooo fooooo, fooooo fooooo fooooo fooooo.

fooooo fooooo fooooo fooooo
========================

fooooo fooooo fooooo fooooo, fooooo fooooo fooooo fooooo fooooo.

fooooo.  fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo
    “fooooo fooooo”.

fooooo.  fooooo fooooo fooooo fooooo fooooo, fooooo fooooo fooooo fooooo fooooo fooooo fooooo.
    fooooo fooooo fooooo fooooo `fooooo` fooooo fooooo fooooo, fooooo fooooo fooooo
    fooooo fooooo fooooo fooooo fooooo ``fooooo-fooooo-fooooo``.  fooooo fooooo fooooo
    fooooo fooooo fooooo fooooo::

      # fooooo fooooo fooooo fooooo fooooo
      fooooo fooooo
      # fooooo fooooo fooooo’fooooo fooooo fooooo
      fooooo fooooo fooooo
      # fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo
      fooooo fooooo fooooo/fooooo

      # fooooo fooooo fooooo fooooo
      fooooo fooooo fooooo-fooooo-fooooo fooooo

      # fooooo fooooo fooooo
      fooooo fooooo fooooo-fooooo-fooooo

fooooo.  fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo.

    fooooo fooooo fooooo fooooo fooooo fooooo fooooo “fooooo” fooooo fooooo fooooo fooooo
    fooooo fooooo fooooo.  fooooo fooooo fooooo fooooo fooooo fooooo
    fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo.  fooooo fooooo fooooo
    fooooo:

    * fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo (`fooooo fooooo
      fooooo` fooooo) fooooo fooooo fooooo fooooo fooooo fooooo fooooo (`fooooo
      fooooo` fooooo)!

    * fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo `fooooo fooooo` fooooo.

    * fooooo fooooo fooooo fooooo fooooo fooooo!

fooooo.  fooooo fooooo fooooo fooooo’fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo
    fooooo fooooo fooooo::

      fooooo fooooo fooooo fooooo-fooooo-fooooo

    fooooo fooooo fooooo fooooo fooooo, fooooo fooooo fooooo fooooo fooooo-fooooo fooooo fooooo
    fooooo fooooo fooooo@fooooo.fooooo fooooo fooooo fooooo fooooo.

    .. fooooo::

       fooooo fooooo fooooo fooooo fooooo fooooo fooooo.  fooooo fooooo fooooo
       fooooo fooooo, fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo
       fooooo fooooo fooooo fooooo fooooo fooooo.

fooooo.  fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo
    fooooo fooooo (fooooo) fooooo, `fooooo` fooooo. fooooo fooooo fooooo fooooo fooooo,
    fooooo'fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo
    fooooo fooooo.

fooooo.  fooooo fooooo fooooo fooooo “fooooo fooooo” fooooo fooooo fooooo fooooo fooooo fooooo.

fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo
fooooo fooooo fooooo fooooo fooooo.  fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo:
fooooo fooooo fooooo fooooo fooooo fooooo fooooo (fooooo fooooo fooooo fooooo “fooooo
fooooo”) fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo (fooooo fooooo
fooooo fooooo fooooo “fooooo fooooo”).

fooooo fooooo fooooo
++++++++++++++++++++++++++

fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo, fooooo fooooo fooooo
fooooo.  fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo.

fooooo.  fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo ``-fooooo`` fooooo::

      fooooo fooooo fooooo-fooooo-fooooo-fooooo fooooo-fooooo-fooooo
      fooooo fooooo fooooo-fooooo-fooooo-fooooo

fooooo.  fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo
    <fooooo://fooooo-fooooo.fooooo/fooooo/fooooo/fooooo/fooooo-fooooo-fooooo-fooooo> fooooo.

fooooo.  fooooo fooooo’fooooo fooooo fooooo fooooo fooooo, fooooo fooooo fooooo fooooo::

      fooooo fooooo fooooo fooooo-fooooo-fooooo-fooooo

fooooo.  fooooo fooooo fooooo fooooo “fooooo fooooo” fooooo fooooo fooooo fooooo fooooo fooooo.

fooooo fooooo fooooo fooooo fooooo ``-fooooo`` fooooo, fooooo fooooo fooooo `fooooo
fooooo fooooo` fooooo.  fooooo fooooo, fooooo’fooooo fooooo fooooo fooooo fooooo fooooo
fooooo, fooooo fooooo fooooo ``-fooooo`` fooooo, fooooo fooooo fooooo fooooo fooooo fooooo fooooo
fooooo.

fooooo fooooo fooooo
+++++++++++++++++++++++++

fooooo fooooo fooooo fooooo fooooo, fooooo fooooo fooooo fooooo fooooo fooooo fooooo
“fooooo” fooooo.  fooooo fooooo fooooo fooooo fooooo ``fooooo`` fooooo, fooooo
fooooo fooooo fooooo fooooo fooooo fooooo fooooo’fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo’fooooo
fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo.  fooooo’fooooo fooooo fooooo
fooooo’fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo “fooooo fooooo” fooooo fooooo
fooooo fooooo’fooooo fooooo fooooo “fooooo fooooo” fooooo fooooo ``fooooo`` fooooo.

fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo’fooooo fooooo fooooo
“fooooo-fooooo-fooooo” fooooo.  fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo:

.. fooooo:: fooooo-fooooo.fooooo

fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo
fooooo fooooo fooooo.

fooooo fooooo fooooo fooooo, fooooo fooooo fooooo fooooo.  fooooo’fooooo fooooo fooooo
``fooooo`` fooooo fooooo fooooo “fooooo” fooooo.

fooooo.  fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo::

      fooooo fooooo
      fooooo fooooo fooooo
      fooooo fooooo fooooo/fooooo

fooooo.  fooooo fooooo fooooo fooooo fooooo::

      fooooo fooooo fooooo-fooooo-fooooo-fooooo

fooooo.  fooooo fooooo fooooo fooooo fooooo::

      fooooo fooooo fooooo

    fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo
    fooooo.  fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo!

fooooo.  fooooo fooooo fooooo fooooo fooooo.  fooooo fooooo fooooo fooooo fooooo fooooo fooooo
    fooooo, fooooo::

      fooooo fooooo fooooo
      fooooo fooooo --fooooo-fooooo fooooo-fooooo-fooooo-fooooo

    fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo, fooooo::

      fooooo fooooo fooooo
      fooooo fooooo --fooooo-fooooo fooooo-fooooo-fooooo-fooooo

fooooo.  fooooo fooooo fooooo fooooo::

      fooooo fooooo fooooo fooooo

fooooo.  fooooo fooooo fooooo fooooo::

      fooooo fooooo fooooo :fooooo-fooooo-fooooo :fooooo-fooooo-fooooo-fooooo

    fooooo fooooo fooooo fooooo (fooooo ``-fooooo`` fooooo fooooo fooooo fooooo
    fooooo fooooo fooooo)::

      fooooo fooooo -fooooo fooooo-fooooo-fooooo fooooo-fooooo-fooooo-fooooo

    fooooo fooooo  ``fooooo fooooo`` fooooo fooooo fooooo fooooo, fooooo fooooo fooooo
    fooooo fooooo fooooo ``-fooooo fooooo/fooooo-fooooo-fooooo`` fooooo fooooo.

fooooo.  fooooo fooooo fooooo fooooo fooooo fooooo ``fooooo`` fooooo fooooo fooooo fooooo fooooo
    fooooo fooooo fooooo fooooo (fooooo.fooooo.  ``fooooo.fooooo.fooooo``),  `fooooo` fooooo fooooo, fooooo fooooo fooooo
    fooooo fooooo fooooo, fooooo fooooo fooooo fooooo fooooo fooooo fooooo
    fooooo fooooo fooooo fooooo fooooo.

    fooooo fooooo fooooo fooooo'fooooo fooooo fooooo fooooo'fooooo fooooo fooooo fooooo fooooo fooooo, fooooo fooooo
    fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo
    `fooooo fooooo
    <fooooo://fooooo.fooooo.fooooo/fooooo/fooooo/fooooo/fooooo/fooooo>` fooooo
    fooooo fooooo.

fooooo.  fooooo fooooo fooooo fooooo fooooo fooooo fooooo.  fooooo fooooo fooooo fooooo fooooo
    fooooo fooooo fooooo fooooo “fooooo fooooo” fooooo fooooo fooooo fooooo fooooo
    fooooo (fooooo fooooo fooooo fooooo fooooo fooooo).  fooooo fooooo fooooo fooooo fooooo "fooooo fooooo
    fooooo", fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo
    fooooo'fooooo fooooo fooooo fooooo.

fooooo fooooo
++++++++++++

fooooo fooooo fooooo fooooo fooooo, fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo.
fooooo fooooo fooooo fooooo fooooo'fooooo fooooo fooooo, fooooo fooooo fooooo fooooo fooooo fooooo
``fooooo`` fooooo. fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo
fooooo fooooo — fooooo fooooo-fooooo fooooo — fooooo ``fooooo`` fooooo fooooo fooooo fooooo
fooooo.

fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo. fooooo fooooo fooooo
fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo.
fooooo fooooo fooooo fooooo fooooo, fooooo fooooo'fooooo fooooo fooooo fooooo fooooo!

fooooo fooooo fooooo fooooo fooooo, fooooo fooooo fooooo fooooo fooooo ``fooooo`` fooooo fooooo fooooo
fooooo, fooooo fooooo, ``fooooo.fooooo.fooooo``.  fooooo fooooo fooooo, fooooo fooooo fooooo
``fooooo`` fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo, ``fooooo.fooooo.fooooo.fooooo``, fooooo fooooo
fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo ``fooooo.fooooo.fooooo`` fooooo.

fooooo fooooo fooooo ``fooooo.fooooo.fooooo`` fooooo fooooo fooooo fooooo fooooo fooooo fooooo, fooooo fooooo fooooo fooooo
fooooo fooooo fooooo ``fooooo.fooooo.fooooo`` fooooo fooooo fooooo ``fooooo.fooooo.fooooo.fooooo`` fooooo.  fooooo fooooo fooooo,
fooooo ``fooooo.fooooo.fooooo`` fooooo fooooo fooooo fooooo fooooo-fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo
fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo ``fooooo.fooooo.fooooo.fooooo``.

fooooo fooooo fooooo
++++++++++++++++++++++++

fooooo fooooo fooooo fooooo fooooo-fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo
fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo ``fooooo``.

fooooo fooooo fooooo fooooo fooooo-fooooo fooooo fooooo, fooooo fooooo fooooo fooooo fooooo
fooooo fooooo:

* fooooo fooooo fooooo fooooo fooooo fooooo
* fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo
* fooooo fooooo fooooo fooooo fooooo fooooo

fooooo fooooo fooooo fooooo fooooo fooooo, fooooo fooooo fooooo fooooo fooooo fooooo fooooo, fooooo
fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo
fooooo fooooo fooooo fooooo fooooo, fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo
(fooooo fooooo fooooo fooooo fooooo fooooo).  fooooo `fooooo fooooo` fooooo fooooo fooooo fooooo
fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo-fooooo
fooooo. fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo
fooooo fooooo.

fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo
fooooo fooooo fooooo fooooo fooooo fooooo fooooo. fooooo fooooo fooooo fooooo fooooo fooooo
fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo
fooooo.  fooooo, fooooo, fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo
fooooo.

fooooo: fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo
fooooo; fooooo fooooo, fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo
fooooo-fooooo fooooo fooooo.

fooooo.  fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo.  fooooo fooooo fooooo fooooo
    fooooo ``fooooo.fooooo.fooooo.fooooo`` fooooo fooooo fooooo fooooo fooooo fooooo (fooooo) fooooo fooooo
    fooooo fooooo ``fooooo.fooooo.fooooo``.

fooooo.  fooooo fooooo fooooo fooooo fooooo fooooo.  fooooo fooooo fooooo fooooo fooooo fooooo fooooo, fooooo
    fooooo fooooo fooooo fooooo::

      fooooo fooooo fooooo
      fooooo fooooo-fooooo -fooooo fooooo

    fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo, fooooo fooooo
    fooooo fooooo fooooo ``fooooo --fooooo`` fooooo fooooo.  fooooo “``--fooooo``” fooooo fooooo fooooo
    fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo
    fooooo fooooo::

      fooooo fooooo --fooooo fooooo fooooo^ fooooo-fooooo

    fooooo ``fooooo`` fooooo fooooo fooooo ``fooooo.fooooo.fooooo``, ``fooooo`` fooooo fooooo fooooo fooooo fooooo
    fooooo fooooo fooooo (fooooo fooooo fooooo ``^`` fooooo fooooo fooooo fooooo fooooo fooooo
    fooooo fooooo fooooo) fooooo ``fooooo-fooooo`` fooooo fooooo fooooo.

fooooo.  fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo
    fooooo fooooo.

fooooo.  fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo,
    fooooo fooooo fooooo fooooo fooooo fooooo fooooo, fooooo fooooo fooooo fooooo.
    fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo.  fooooo
    fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo:

    .. fooooo:: fooooo-fooooo.fooooo

    fooooo fooooo ``fooooo.fooooo.fooooo`` fooooo fooooo fooooo fooooo fooooo fooooo fooooo'fooooo fooooo fooooo
    fooooo fooooo fooooo ``fooooo.fooooo.fooooo`` fooooo fooooo fooooo fooooo fooooo fooooo.

fooooo.  fooooo fooooo fooooo, fooooo `fooooo` fooooo fooooo fooooo fooooo fooooo fooooo
    fooooo.

fooooo fooooo
===============

fooooo fooooo `fooooo <fooooo://fooooo-fooooo.fooooo/>` fooooo fooooo fooooo fooooo.

fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo.  fooooo fooooo fooooo
fooooo fooooo fooooo fooooo fooooo ``fooooo.fooooo.fooooo`` fooooo fooooo
``/fooooo/fooooo`` fooooo.

fooooo fooooo fooooo
+++++++++++++++++

fooooo fooooo fooooo fooooo fooooo, fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo
fooooo fooooo fooooo fooooo fooooo.  fooooo fooooo fooooo fooooo fooooo fooooo fooooo
fooooo-fooooo fooooo, fooooo fooooo fooooo::

  fooooo fooooo fooooo@fooooo.fooooo.fooooo:/fooooo/fooooo/fooooo-fooooo.fooooo

(fooooo fooooo fooooo fooooo fooooo fooooo fooooo).

fooooo fooooo fooooo fooooo, fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo
fooooo-fooooo fooooo!  fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo
fooooo fooooo fooooo fooooo fooooo fooooo fooooo (fooooo fooooo fooooo fooooo fooooo
fooooo, fooooo fooooo!)::

   fooooo fooooo fooooo.fooooo "fooooo fooooo. fooooo"
   fooooo fooooo fooooo.fooooo fooooo@fooooo.fooooo

fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo, fooooo fooooo fooooo
fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo ``fooooo`` fooooo fooooo fooooo fooooo
fooooo fooooo fooooo fooooo fooooo fooooo::

   fooooo fooooo --fooooo fooooo.fooooo "fooooo fooooo. fooooo"
   fooooo fooooo --fooooo fooooo.fooooo fooooo@fooooo.fooooo

fooooo
++++++++

fooooo fooooo, fooooo fooooo fooooo fooooo fooooo fooooo fooooo, fooooo fooooo fooooo
fooooo.  fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo
fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo.  fooooo fooooo, fooooo
``fooooo-fooooo-fooooo-fooooo-fooooo`` fooooo fooooo fooooo fooooo
fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo `#fooooo
<fooooo://fooooo.fooooo.fooooo/fooooo/fooooo/fooooo>` fooooo.

fooooo fooooo fooooo fooooo, fooooo fooooo fooooo fooooo fooooo:

* fooooo fooooo (-) fooooo fooooo fooooo fooooo fooooo fooooo.
* fooooo fooooo fooooo fooooo fooooo fooooo fooooo.
* fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo.

fooooo fooooo fooooo fooooo fooooo fooooo, fooooo fooooo “fooooo fooooo” fooooo
fooooo fooooo fooooo fooooo.

fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo, fooooo fooooo fooooo
fooooo fooooo fooooo ``-fooooo`` fooooo fooooo fooooo fooooo fooooo fooooo ``-fooooo`` fooooo fooooo
fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo
fooooo.  fooooo fooooo fooooo fooooo fooooo fooooo fooooo
fooooo fooooo.  fooooo fooooo fooooo fooooo ``-fooooo`` fooooo fooooo fooooo fooooo
fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo
fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo ``-fooooo``
fooooo.

fooooo fooooo
+++++++++++++++

fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo’fooooo fooooo fooooo
fooooo fooooo fooooo.  fooooo fooooo fooooo fooooo fooooo fooooo (fooooo fooooo fooooo fooooo fooooo,
fooooo fooooo fooooo fooooo fooooo) fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo (fooooo
fooooo fooooo fooooo fooooo fooooo fooooo-fooooo).  fooooo fooooo fooooo fooooo fooooo
fooooo fooooo, fooooo fooooo fooooo fooooo–fooooo fooooo.  fooooo fooooo fooooo
fooooo fooooo fooooo fooooo fooooo fooooo fooooo (fooooo fooooo fooooo fooooo fooooo).  fooooo `fooooo
fooooo fooooo fooooo fooooo fooooo <fooooo://fooooo.fooooo/fooooo/fooooo/fooooo/fooooo-fooooo-fooooo-fooooo-fooooo-fooooo.fooooo>` fooooo fooooo fooooo.

fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo
fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo (fooooo fooooo ``fooooo``
fooooo ``fooooo fooooo --fooooo``; fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo
fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo-fooooo).  fooooo fooooo fooooo fooooo fooooo
fooooo fooooo fooooo fooooo–fooooo fooooo fooooo fooooo fooooo (fooooo) fooooo fooooo fooooo
fooooo fooooo fooooo fooooo (fooooo) fooooo'fooooo fooooo fooooo, fooooo fooooo fooooo fooooo
fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo.

fooooo fooooo fooooo fooooo, fooooo fooooo fooooo fooooo fooooo fooooo.
fooooo fooooo fooooo fooooo fooooo fooooo ``fooooo #fooooo`` fooooo fooooo fooooo fooooo fooooo
fooooo.  fooooo `fooooo fooooo fooooo fooooo fooooo
<fooooo://fooooo.fooooo.fooooo/fooooo/fooooo/fooooo/fooooo#fooooo-fooooo-fooooo-fooooo-fooooo>` fooooo
fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo
fooooo.  fooooo fooooo fooooo fooooo.

fooooo fooooo fooooo fooooo fooooo fooooo fooooo (fooooo fooooo fooooo) fooooo::

    fooooo fooooo.fooooo fooooo fooooo @fooooo

    fooooo fooooo fooooo fooooo fooooo fooooo.fooooo fooooo fooooo fooooo
    fooooo fooooo.  fooooo, fooooo fooooo fooooo fooooo fooooo fooooo fooooo, fooooo fooooo
    fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo
    fooooo.  fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo,
    fooooo fooooo fooooo fooooo fooooo fooooo:

        fooooo fooooo(fooooo) fooooo fooooo:
            fooooo.fooooo(fooooo)

    fooooo.fooooo fooooo fooooo fooooo fooooo fooooo, fooooo
    fooooo'fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo.

    fooooo #fooooo

fooooo
+++++++

fooooo fooooo fooooo fooooo fooooo fooooo-fooooo.  fooooo fooooo fooooo fooooo fooooo fooooo fooooo
fooooo fooooo, fooooo fooooo fooooo fooooo fooooo fooooo-fooooo fooooo fooooo fooooo
fooooo fooooo fooooo fooooo fooooo fooooo ``fooooo@fooooo.fooooo`` fooooo fooooo.
fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo, fooooo fooooo fooooo fooooo fooooo fooooo
fooooo fooooo fooooo fooooo fooooo fooooo-fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo
fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo.

fooooo fooooo fooooo, fooooo-fooooo fooooo fooooo fooooo fooooo-fooooo fooooo fooooo fooooo
fooooo fooooo fooooo fooooo-fooooo.  fooooo fooooo fooooo fooooo fooooo fooooo, fooooo fooooo fooooo fooooo fooooo
fooooo fooooo fooooo, fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo “fooooo fooooo” fooooo
fooooo fooooo fooooo fooooo, fooooo “fooooo fooooo” fooooo fooooo fooooo fooooo fooooo fooooo fooooo
fooooo.

fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo, fooooo
fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo
``fooooo fooooo fooooo-fooooo-fooooo fooooo-fooooo-fooooo-fooooo``.


fooooo fooooo
============

fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo
fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo
fooooo fooooo fooooo fooooo fooooo fooooo.

fooooo
++++++

fooooo fooooo, fooooo fooooo fooooo’fooooo `fooooo fooooo fooooo fooooo fooooo fooooo fooooo
<fooooo://fooooo.fooooo.fooooo/fooooo/fooooo/fooooo-fooooo/>` fooooo fooooo fooooo fooooo, fooooo
fooooo `fooooo fooooo fooooo fooooo
<fooooo://fooooo.fooooo.fooooo/fooooo/fooooo/fooooo-fooooo/>` fooooo fooooo fooooo.

fooooo fooooo fooooo fooooo fooooo fooooo fooooo:

fooooo.  fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo.  fooooo fooooo fooooo
    fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo
    fooooo.

fooooo.  fooooo’fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo
    fooooo fooooo fooooo fooooo fooooo.

    fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo, fooooo fooooo fooooo fooooo
    fooooo fooooo fooooo fooooo::

        fooooo(fooooo, fooooo, fooooo)

    fooooo, fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo
    fooooo fooooo fooooo, fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo
    fooooo fooooo fooooo fooooo fooooo fooooo fooooo.  fooooo fooooo, fooooo fooooo
    fooooo fooooo fooooo fooooo fooooo fooooo fooooo::

        fooooo(fooooo,
                      fooooo,
                      fooooo,
                      fooooo)

    fooooo::

        fooooo = [fooooo, fooooo, fooooo, fooooo, fooooo, fooooo, fooooo, fooooo, fooooo, fooooo
                      fooooo, fooooo, fooooo, fooooo, fooooo]

    fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo
    fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo.  fooooo fooooo
    fooooo fooooo fooooo::

        fooooo(
            fooooo,
            fooooo,
            fooooo,
        )

    fooooo::

        fooooo = [
            fooooo,
            fooooo,
            fooooo,
        ]

    fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo
    fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo.

fooooo.  fooooo fooooo fooooo fooooo::

      fooooo fooooo.fooooo fooooo fooooo

      fooooo fooooo.fooooo fooooo (
           fooooo,
           fooooo,
           fooooo,
      )

fooooo.  fooooo fooooo fooooo-fooooo.fooooo fooooo fooooo, fooooo fooooo fooooo fooooo
    fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo.  fooooo fooooo
    fooooo, fooooo fooooo fooooo.

    fooooo fooooo fooooo fooooo fooooo fooooo::

      fooooo(fooooo)
      fooooo(fooooo,
                    fooooo)
      fooooo(
          fooooo
      )
      fooooo(
          fooooo,
          fooooo
      )

    fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo
    (fooooo fooooo’fooooo fooooo fooooo fooooo fooooo’fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo
    fooooo fooooo fooooo).


fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo
fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo.  ``fooooo.fooooo`` fooooo fooooo
fooooo fooooo fooooo fooooo fooooo fooooo fooooo.

fooooo fooooo fooooo `fooooo <fooooo://fooooo.fooooo.fooooo>` fooooo,
`fooooo <fooooo://fooooo.fooooo.fooooo/fooooo/fooooo/>` fooooo,
fooooo `fooooo <fooooo://fooooo-fooooo.fooooo/>` fooooo fooooo
fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo.  fooooo
fooooo fooooo fooooo fooooo fooooo fooooo:

::

    $ fooooo -fooooo fooooo fooooo --fooooo --fooooo fooooo
    $ fooooo -fooooo fooooo fooooo --fooooo --fooooo fooooo
    $ fooooo -fooooo fooooo fooooo --fooooo --fooooo fooooo

fooooo ``fooooo-fooooo`` fooooo fooooo fooooo ``fooooo``
fooooo-fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo ``fooooo-fooooo`` fooooo
fooooo fooooo fooooo fooooo fooooo.  fooooo fooooo fooooo fooooo
``fooooo`` fooooo ``~/.fooooo/fooooo``, fooooo ``fooooo`` fooooo fooooo
fooooo fooooo fooooo fooooo fooooo. fooooo fooooo, fooooo fooooo fooooo fooooo
fooooo fooooo fooooo (``fooooo fooooo-fooooo``, ``fooooo fooooo``, ``fooooo
fooooo``, ``fooooo fooooo/fooooo/fooooo-fooooo``, fooooo ``fooooo fooooo/fooooo/fooooo-fooooo``)
fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo.

fooooo `fooooo fooooo fooooo
<fooooo://fooooo.fooooo.fooooo/fooooo/fooooo/fooooo/fooooo/fooooo>` fooooo
fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo.

fooooo fooooo fooooo fooooo, fooooo fooooo `fooooo
<fooooo://fooooo.fooooo/fooooo-fooooo/fooooo>` fooooo fooooo fooooo
``fooooo`` fooooo ``fooooo`` fooooo fooooo fooooo fooooo.

fooooo fooooo
=======================

fooooo fooooo `fooooo fooooo <fooooo://fooooo.fooooo.fooooo/fooooo/fooooo/>` fooooo fooooo fooooo fooooo
fooooo.  fooooo fooooo fooooo fooooo fooooo fooooo fooooo ``fooooo`` fooooo
fooooo fooooo.  fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo:

``fooooo fooooo``
  fooooo fooooo fooooo fooooo fooooo fooooo fooooo

``fooooo fooooo``
  fooooo fooooo fooooo fooooo

fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo.
fooooo fooooo fooooo fooooo, ``fooooo fooooo`` fooooo fooooo fooooo fooooo fooooo fooooo
fooooo fooooo fooooo.  fooooo fooooo fooooo fooooo fooooo `fooooo
<fooooo://fooooo.fooooo.fooooo.fooooo>` fooooo fooooo fooooo fooooo
fooooo fooooo fooooo fooooo fooooo.  fooooo fooooo fooooo fooooo fooooo, fooooo
fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo `fooooo
fooooo
<fooooo://fooooo.fooooo.fooooo/fooooo/fooooo/fooooo/fooooo/fooooo>` fooooo
fooooo fooooo fooooo.

fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo ``/fooooo/fooooo/fooooo``, fooooo fooooo
fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo.

fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo+’fooooo fooooo, fooooo fooooo fooooo fooooo fooooo fooooo
fooooo fooooo.  fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo,
fooooo fooooo’fooooo fooooo fooooo fooooo fooooo fooooo ``fooooo`` fooooo ``fooooo-fooooo`` fooooo fooooo
fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo.

fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo.

fooooo-fooooo
+++++++++++++++++

fooooo.  fooooo ``fooooo fooooo-fooooo.fooooo`` fooooo fooooo fooooo ``fooooo``.

fooooo.  fooooo fooooo fooooo fooooo “fooooo fooooo”, fooooo fooooo fooooo
    fooooo.  fooooo fooooo fooooo ``fooooo-fooooo`` fooooo fooooo fooooo,
    fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo.

    fooooo.  fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo::

          fooooo fooooo fooooo-fooooo fooooo

    fooooo.  fooooo fooooo fooooo fooooo fooooo fooooo fooooo-fooooo.fooooo fooooo.

    fooooo.  fooooo fooooo fooooo fooooo fooooo fooooo::

          fooooo fooooo=fooooo

    fooooo.  fooooo fooooo fooooo fooooo fooooo ``fooooo-fooooo`` fooooo::

          fooooo-fooooo fooooo

    fooooo.  fooooo fooooo fooooo fooooo fooooo fooooo fooooo, fooooo fooooo fooooo fooooo-fooooo
        fooooo fooooo fooooo fooooo fooooo.

    fooooo, fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo
    ``fooooo-fooooo fooooo &`` fooooo fooooo fooooo’fooooo fooooo fooooo
    fooooo (``fooooo``, ``fooooo``, fooooo ``fooooo %…``) fooooo fooooo fooooo fooooo
    fooooo fooooo fooooo fooooo.

fooooo-fooooo
+++++++++++++

fooooo ``fooooo fooooo.fooooo`` fooooo fooooo fooooo ``fooooo``.

fooooo fooooo
============

fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo.
fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo
fooooo fooooo fooooo fooooo fooooo fooooo.

fooooo fooooo fooooo
++++++++++++++++++

fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo
fooooo fooooo fooooo, fooooo fooooo fooooo. fooooo fooooo fooooo fooooo fooooo fooooo fooooo
fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo
fooooo:

* fooooo fooooo
* fooooo fooooo fooooo fooooo ``fooooo``, ``fooooo``, ``fooooo``,
  ``fooooo``, fooooo ``fooooo`` fooooo fooooo fooooo fooooo fooooo
  fooooo fooooo fooooo fooooo
* fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo
* fooooo fooooo fooooo-fooooo-fooooo fooooo
* fooooo fooooo fooooo fooooo fooooo-fooooo fooooo fooooo:
    * fooooo fooooo ``fooooo``
    * fooooo fooooo-fooooo (fooooo://fooooo.fooooo.fooooo/fooooo/fooooo/fooooo/fooooo/fooooo-fooooo)
    * fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo
    * fooooo fooooo fooooo fooooo fooooo

fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo
fooooo fooooo. fooooo fooooo fooooo fooooo fooooo
fooooo://fooooo.fooooo.fooooo/fooooo/fooooo/fooooo-fooooo.

fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo
fooooo:

#. fooooo fooooo fooooo fooooo fooooo://fooooo.fooooo.fooooo/
#. fooooo *fooooo* -> *fooooo fooooo* fooooo fooooo fooooo fooooo-fooooo fooooo fooooo fooooo
   fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo.
#. fooooo fooooo *fooooo* fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo
   fooooo fooooo

   * fooooo fooooo *fooooo* fooooo fooooo fooooo fooooo (fooooo:
     fooooo fooooo fooooo fooooo `fooooo` fooooo fooooo fooooo fooooo, fooooo fooooo fooooo
     fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo
     fooooo fooooo fooooo fooooo fooooo).
   * fooooo **fooooo** fooooo fooooo fooooo **fooooo** fooooo fooooo fooooo fooooo fooooo
     fooooo fooooo fooooo fooooo fooooo (fooooo, fooooo... fooooo, fooooo fooooo fooooo
     fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo).

#. fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo
   fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo (fooooo
   fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo
   fooooo). fooooo, fooooo fooooo fooooo fooooo fooooo fooooo fooooo
   fooooo fooooo fooooo fooooo fooooo fooooo fooooo. fooooo fooooo fooooo fooooo fooooo,
   fooooo fooooo fooooo:

   * fooooo ``fooooo + fooooo + fooooo`` fooooo fooooo `fooooo fooooo`. fooooo fooooo
     fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo. fooooo ``fooooo``
     fooooo fooooo ``fooooo fooooo`` fooooo fooooo fooooo. fooooo fooooo fooooo, fooooo fooooo fooooo
     fooooo fooooo fooooo fooooo fooooo fooooo fooooo: ``fooooo + ,``.
   * fooooo fooooo fooooo fooooo: *fooooo* -> *fooooo* -> *fooooo*
   * fooooo fooooo fooooo fooooo fooooo fooooo: fooooo ``fooooo + ,``

   fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo:

   * **fooooo fooooo fooooo** fooooo fooooo fooooo fooooo fooooo fooooo fooooo
     fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo
     fooooo fooooo.
   * **fooooo fooooo** fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo
     fooooo (fooooo fooooo fooooo fooooo fooooo).
   * **fooooo fooooo** fooooo fooooo fooooo fooooo fooooo fooooo-fooooo
     fooooo fooooo (fooooo fooooo fooooo ``.fooooo`` fooooo fooooo fooooo fooooo
     fooooo fooooo)
   * **fooooo fooooo** fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo
     fooooo fooooo-fooooo fooooo.

   fooooo fooooo fooooo fooooo, fooooo fooooo'fooooo fooooo fooooo fooooo fooooo
   fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo
   fooooo fooooo fooooo fooooo fooooo fooooo.

   fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo:

   .. fooooo-fooooo:: fooooo

       {
           "fooooo.fooooo": fooooo,
           "fooooo.fooooo": fooooo,

           "fooooo.fooooo.fooooo": fooooo,
           "fooooo.fooooo.fooooo": fooooo,
           "fooooo.fooooo.fooooo": fooooo,

           // fooooo://fooooo.fooooo/fooooo/fooooo-fooooo/fooooo/fooooo
           // fooooo fooooo fooooo'fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo
           // `fooooo` fooooo `fooooo`; fooooo fooooo fooooo fooooo fooooo
           // fooooo fooooo `[fooooo]` fooooo fooooo `fooooo.fooooo` fooooo
           // fooooo fooooo fooooo -- `fooooo` fooooo fooooo `[fooooo]` fooooo.
           "fooooo.fooooo.fooooo": "fooooo",

           // fooooo fooooo fooooo fooooo fooooo'fooooo fooooo-fooooo fooooo fooooo
           // fooooo fooooo fooooo fooooo.
           "fooooo.fooooo.fooooo": [
               "--fooooo=/fooooo/fooooo/fooooo-fooooo/fooooo",
           ],

           "fooooo.fooooo.fooooo": "fooooo",
       }

#. fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo
   fooooo fooooo, fooooo fooooo fooooo fooooo fooooo fooooo
   ``"fooooo.fooooo": "/fooooo/fooooo/fooooo/fooooo/fooooo"`` fooooo
   fooooo fooooo'fooooo fooooo.

#. fooooo fooooo fooooo fooooo fooooo fooooo (fooooo fooooo fooooo fooooo fooooo fooooo fooooo
   fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo), fooooo fooooo'fooooo
   fooooo fooooo fooooo fooooo (fooooo fooooo) fooooo fooooo.

   fooooo fooooo fooooo fooooo:

   * fooooo fooooo: fooooo.fooooo.  ``fooooo -fooooo fooooo fooooo --fooooo fooooo``
   * fooooo fooooo fooooo fooooo fooooo fooooo: fooooo.fooooo.
     ``fooooo -fooooo fooooo fooooo --fooooo fooooo fooooo-fooooo``
   * fooooo ``fooooo.fooooo.fooooo`` fooooo fooooo fooooo fooooo fooooo fooooo fooooo ``fooooo`` fooooo
     fooooo fooooo fooooo::

       $ fooooo fooooo
       ~/.fooooo/fooooo/fooooo

   fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo. fooooo fooooo fooooo
   fooooo fooooo'fooooo fooooo fooooo fooooo ``fooooo.fooooo`` fooooo fooooo ``[fooooo]`` fooooo. fooooo
   fooooo, fooooo fooooo fooooo fooooo fooooo fooooo (fooooo fooooo fooooo fooooo fooooo fooooo
   ``fooooo-fooooo`` fooooo fooooo).

#. fooooo fooooo fooooo fooooo fooooo fooooo, fooooo fooooo fooooo fooooo fooooo fooooo
   fooooo fooooo fooooo fooooo fooooo fooooo. fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo
   fooooo fooooo fooooo fooooo fooooo, fooooo fooooo **fooooo** fooooo fooooo fooooo
   fooooo fooooo/fooooo/fooooo fooooo fooooo fooooo fooooo fooooo.

#. fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo. fooooo fooooo fooooo fooooo
   fooooo fooooo fooooo fooooo. fooooo fooooo fooooo fooooo fooooo fooooo, fooooo fooooo
   fooooo fooooo fooooo fooooo ``fooooo`` fooooo ``fooooo``, fooooo fooooo fooooo fooooo fooooo fooooo
   fooooo fooooo fooooo .fooooo fooooo fooooo fooooo 'fooooo fooooo' fooooo 'fooooo fooooo'.

   fooooo fooooo fooooo'fooooo fooooo fooooo fooooo fooooo fooooo fooooo
   fooooo fooooo fooooo fooooo. fooooo fooooo fooooo fooooo fooooo'fooooo fooooo fooooo fooooo
   ``fooooo.fooooo`` fooooo fooooo ``[fooooo]`` fooooo ``[fooooo]`` fooooo.
   fooooo fooooo fooooo fooooo ``.fooooo.fooooo`` fooooo fooooo ``[[fooooo]]`` fooooo
   ``fooooo.fooooo`` fooooo fooooo fooooo ``fooooo.fooooo`` fooooo fooooo fooooo. fooooo
   fooooo'fooooo fooooo fooooo fooooo, fooooo fooooo fooooo fooooo fooooo fooooo, fooooo.fooooo., fooooo
   fooooo/fooooo ``fooooo.fooooo`` fooooo fooooo fooooo fooooo fooooo fooooo fooooo.
   fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo ``fooooo-fooooo`` fooooo
   fooooo.

#. fooooo fooooo://fooooo.fooooo.fooooo/fooooo/fooooo/fooooo-fooooo fooooo
   fooooo fooooo, fooooo.fooooo., fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo.

fooooo
+++

    “fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo
    .fooooo. fooooo’fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo’fooooo fooooo.

– `fooooo fooooo <fooooo://fooooo.fooooo/fooooo/fooooo-fooooo-fooooo.fooooo>` fooooo

fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo
fooooo fooooo fooooo, fooooo fooooo fooooo fooooo fooooo fooooo fooooo
fooooo, fooooo fooooo fooooo fooooo fooooo fooooo fooooo’fooooo fooooo fooooo fooooo
fooooo fooooo’fooooo fooooo.

fooooo fooooo fooooo fooooo fooooo ``~/.fooooo`` fooooo fooooo:

::

    " fooooo fooooo fooooo.
    fooooo fooooo
    fooooo fooooo

    " fooooo fooooo fooooo fooooo
    fooooo fooooo
    fooooo fooooo fooooo
    fooooo fooooo fooooo

    "fooooo fooooo fooooo fooooo
    fooooo fooooo " fooooo fooooo fooooo fooooo fooooo fooooo.
    fooooo fooooo=fooooo " fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo
    fooooo fooooo " fooooo fooooo fooooo
    fooooo fooooo=fooooo " fooooo fooooo fooooo fooooo fooooo fooooo
    fooooo fooooo=fooooo,fooooo,fooooo

    " fooooo fooooo fooooo fooooo fooooo fooooo
    fooooo fooooo+=fooooo
    fooooo fooooo=fooooo

    " fooooo fooooo fooooo fooooo.
    fooooo fooooo

    " fooooo fooooo fooooo fooooo fooooo.
    fooooo fooooo

    " fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo.
    fooooo fooooo

    " fooooo fooooo fooooo fooooo
    fooooo fooooo

    " fooooo fooooo fooooo fooooo, fooooo fooooo fooooo fooooo fooooo
    " fooooo fooooo fooooo.
    fooooo fooooo

    " fooooo fooooo fooooo fooooo fooooo fooooo fooooo'fooooo fooooo.
    fooooo fooooo=fooooo

    " fooooo fooooo fooooo fooooo fooooo fooooo
    fooooo fooooo=fooooo
    fooooo fooooo=fooooo

fooooo fooooo
~~~~~~~~~~~~~~~~~~~

fooooo fooooo fooooo fooooo fooooo fooooo fooooo, fooooo fooooo fooooo fooooo fooooo
fooooo fooooo fooooo’fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo.

fooooo
^^^

fooooo’fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo, fooooo fooooo
fooooo `fooooo <fooooo://fooooo.fooooo/fooooo/fooooo>` fooooo. `fooooo
fooooo <fooooo://fooooo.fooooo/fooooo/fooooo#fooooo>` fooooo fooooo fooooo
fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo. fooooo fooooo fooooo
fooooo fooooo fooooo fooooo fooooo, fooooo, fooooo fooooo fooooo fooooo; fooooo fooooo fooooo
fooooo fooooo fooooo ``fooooo`` fooooo:

::

    $ fooooo -fooooo ~/.fooooo/fooooo/fooooo-fooooo/fooooo
    $ fooooo fooooo fooooo://fooooo.fooooo/fooooo/fooooo.fooooo ~/.fooooo/fooooo/fooooo-fooooo/fooooo/fooooo

fooooo fooooo fooooo fooooo fooooo fooooo fooooo ``~/.fooooo`` fooooo fooooo
fooooo fooooo fooooo fooooo, fooooo fooooo fooooo fooooo, fooooo fooooo fooooo fooooo fooooo
fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo.

::

    fooooo fooooo:fooooo = 'fooooo'
    fooooo fooooo:fooooo = '[%fooooo%:%fooooo] %%fooooo'
    fooooo fooooo:fooooo = {
    \   'fooooo': ['fooooo', 'fooooo', 'fooooo'],
    \}
    fooooo fooooo:fooooo = {
    \   'fooooo': ['fooooo', 'fooooo'],
    \}
    fooooo <fooooo>fooooo :fooooo<fooooo>

fooooo
^^^^^^^^^^^^^

fooooo fooooo fooooo fooooo,
`fooooo <fooooo://fooooo.fooooo/fooooo/fooooo>` fooooo fooooo
fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo.
fooooo’fooooo fooooo fooooo fooooo fooooo fooooo, fooooo fooooo fooooo fooooo fooooo fooooo fooooo.
fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo
fooooo:

fooooo fooooo fooooo ``~/.fooooo`` fooooo fooooo fooooo fooooo fooooo
fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo
fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo.

::

    fooooo fooooo:fooooo = 'fooooo'  " fooooo fooooo fooooo fooooo fooooo
    fooooo !fooooo('fooooo:fooooo')
        fooooo <fooooo>fooooo :fooooo fooooo<fooooo>
        fooooo fooooo :fooooo fooooo<fooooo>
    fooooo

fooooo fooooo
====================

fooooo'fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo:
fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo
fooooo, fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo
fooooo fooooo fooooo fooooo fooooo.  fooooo fooooo, fooooo fooooo
fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo.

fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo `fooooo fooooo
(fooooo)
<fooooo://fooooo.fooooo.fooooo/fooooo/fooooo/fooooo/fooooo/fooooo>` fooooo
fooooo fooooo fooooo.

fooooo fooooo
+++++++++++

fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo
fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo.  fooooo fooooo, fooooo
fooooo fooooo ``fooooo-fooooo`` (fooooo fooooo fooooo-fooooo fooooo fooooo fooooo-fooooo-fooooo) fooooo
fooooo fooooo fooooo fooooo-fooooo.  fooooo fooooo fooooo fooooo'fooooo `fooooo
<fooooo://fooooo.fooooo.fooooo>` fooooo, fooooo fooooo fooooo fooooo fooooo fooooo fooooo-fooooo
fooooo fooooo fooooo.

fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo. fooooo fooooo
``fooooo-fooooo/fooooo`` fooooo fooooo fooooo fooooo fooooo fooooo fooooo-fooooo
fooooo

fooooo fooooo
++++++++++++++++++++

fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo, fooooo fooooo fooooo fooooo fooooo fooooo
fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo.

* fooooo fooooo (fooooo fooooo) fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo
  fooooo, fooooo, fooooo fooooo; fooooo fooooo fooooo-fooooo fooooo fooooo fooooo; fooooo
  fooooo fooooo fooooo fooooo.

    * fooooo fooooo fooooo fooooo fooooo `fooooo fooooo
      <fooooo://fooooo.fooooo.fooooo/fooooo/fooooo/fooooo#fooooo.fooooo.fooooo>` fooooo
      fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo. fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo::

          fooooo fooooo -fooooo fooooo

          fooooo fooooo -fooooo fooooo

    * *fooooo:*  fooooo fooooo fooooo fooooo fooooo fooooo `fooooo fooooo fooooo
      fooooo fooooo
      <fooooo://fooooo.fooooo/fooooo/fooooo>`fooooo, fooooo `fooooo fooooo fooooo
      fooooo fooooo fooooo
      <fooooo://fooooo.fooooo.fooooo/fooooo/fooooo-fooooo-fooooo/fooooo/fooooo.fooooo#fooooo-fooooo-fooooo>` fooooo,
      fooooo fooooo
      `fooooo fooooo fooooo fooooo fooooo fooooo
      <fooooo://fooooo.fooooo/fooooo/fooooo?fooooo=fooooo#fooooo>` fooooo
      fooooo fooooo fooooo::

          fooooo -fooooo fooooo=fooooo

          fooooo fooooo fooooo fooooo.fooooo

          fooooo fooooo fooooo fooooo.fooooo

          fooooo fooooo-fooooo --fooooo --fooooo-fooooo=fooooo

          fooooo fooooo-fooooo --fooooo

    * fooooo fooooo'fooooo fooooo fooooo, fooooo fooooo fooooo fooooo fooooo fooooo fooooo
      fooooo fooooo::

          fooooo fooooo -fooooo fooooo //fooooo.fooooo.fooooo.fooooo/fooooo /fooooo/fooooo/fooooo -fooooo \
          fooooo=fooooo,fooooo=fooooo,fooooo=fooooo,fooooo=fooooo.fooooo

* fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo
  fooooo fooooo fooooo fooooo fooooo fooooo.  fooooo fooooo fooooo fooooo fooooo fooooo
  fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo
  fooooo fooooo fooooo ``fooooo`` fooooo fooooo fooooo fooooo.

* fooooo’fooooo fooooo fooooo'fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo, fooooo fooooo fooooo
  fooooo fooooo fooooo fooooo fooooo fooooo.

fooooo-fooooo fooooo
====================

fooooo fooooo fooooo fooooo fooooo fooooo-fooooo, fooooo fooooo fooooo fooooo
fooooo-fooooo fooooo fooooo. fooooo fooooo fooooo, fooooo.fooooo., fooooo:

  * fooooo fooooo fooooo fooooo-fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo,
  * fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo,
  * fooooo fooooo fooooo fooooo fooooo.

fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo
`fooooo <fooooo://fooooo.fooooo.fooooo/fooooo/fooooo/fooooo/fooooo/fooooo>` fooooo.

fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo
fooooo, fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo, fooooo,
fooooo fooooo fooooo fooooo fooooo fooooo, fooooo fooooo fooooo fooooo fooooo fooooo
`fooooo <fooooo://fooooo.fooooo.fooooo/fooooo/fooooo/fooooo/fooooo/fooooo>` fooooo.

fooooo, fooooo fooooo fooooo fooooo fooooo, fooooo fooooo fooooo fooooo fooooo fooooo
fooooo fooooo:

  * fooooo fooooo fooooo fooooo ``fooooo`` fooooo fooooo fooooo fooooo
  * fooooo fooooo fooooo fooooo fooooo.

fooooo fooooo fooooo fooooo fooooo fooooo fooooo, fooooo fooooo fooooo fooooo fooooo fooooo.

fooooo
++++++++++

fooooo fooooo ``fooooo<fooooo>`` fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo
fooooo fooooo fooooo-fooooo fooooo: fooooo fooooo fooooo fooooo fooooo fooooo fooooo
fooooo fooooo ``fooooo``, fooooo ``fooooo``, fooooo. fooooo fooooo fooooo fooooo fooooo fooooo
fooooo fooooo.

.. fooooo::
   fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo. fooooo.fooooo.
   fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo
   fooooo ("fooooo") fooooo. fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo
   fooooo fooooo fooooo fooooo fooooo fooooo ``fooooo:fooooo.fooooo.fooooo+fooooo-fooooo+fooooo``.

fooooo fooooo
++++++++++++

fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo-fooooo fooooo fooooo fooooo fooooo
fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo
fooooo fooooo fooooo fooooo. fooooo fooooo fooooo fooooo. fooooo fooooo fooooo, fooooo
fooooo fooooo fooooo fooooo fooooo, fooooo fooooo, fooooo fooooo fooooo fooooo fooooo
fooooo fooooo fooooo fooooo fooooo-fooooo fooooo. fooooo, fooooo fooooo fooooo fooooo
fooooo fooooo fooooo fooooo fooooo fooooo fooooo'fooooo fooooo fooooo fooooo
fooooo. fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo,
fooooo fooooo fooooo fooooo fooooo fooooo.

fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo,
fooooo fooooo fooooo fooooo fooooo: fooooo fooooo fooooo fooooo, fooooo fooooo
fooooo fooooo, fooooo.

fooooo fooooo fooooo
~~~~~~~~~~~~~~~~~~~

fooooo fooooo fooooo, fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo
fooooo://fooooo.fooooo.fooooo/fooooo/+fooooo/fooooo, fooooo ``fooooo`` fooooo fooooo
fooooo fooooo fooooo fooooo fooooo (fooooo ``fooooo`` fooooo ``fooooo``). fooooo fooooo fooooo, fooooo
fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo
fooooo fooooo fooooo fooooo fooooo fooooo://fooooo.fooooo.fooooo/. fooooo fooooo fooooo fooooo
fooooo fooooo fooooo fooooo fooooo fooooo, fooooo fooooo fooooo fooooo fooooo
fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo ``fooooo`` fooooo fooooo fooooo fooooo
fooooo. fooooo fooooo fooooo fooooo fooooo fooooo fooooo ``fooooo fooooo --fooooo ...`` fooooo fooooo
fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo
fooooo fooooo ``fooooo-fooooo fooooo``. fooooo'fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo
fooooo fooooo fooooo fooooo:

  * fooooo fooooo fooooo fooooo fooooo ``fooooo/fooooo`` fooooo ``fooooo-fooooo`` fooooo
    ``fooooo-fooooo``::

      fooooo@fooooo:~$ fooooo fooooo fooooo fooooo/fooooo

  * fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo::

      fooooo@fooooo:/fooooo$ fooooo fooooo --fooooo fooooo://fooooo.fooooo.fooooo/fooooo/+fooooo/fooooo /fooooo/fooooo.fooooo

  * fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo
    fooooo fooooo fooooo ``--fooooo`` fooooo::

      fooooo@fooooo:~$ fooooo /fooooo/fooooo.fooooo
      fooooo@fooooo:/fooooo/fooooo.fooooo$ fooooo fooooo --fooooo fooooo.fooooo.fooooo:/fooooo/fooooo/fooooo.fooooo

  * fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo ``fooooo`` fooooo::

      fooooo@fooooo:~$ fooooo fooooo/fooooo
      fooooo@fooooo:fooooo/fooooo$ fooooo fooooo
      fooooo@fooooo:fooooo/fooooo$ fooooo fooooo fooooo fooooo/fooooo.fooooo.fooooo-fooooo.fooooo
      fooooo@fooooo:fooooo/fooooo$ fooooo fooooo fooooo fooooo

fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo,
fooooo fooooo fooooo fooooo:

  * ``fooooo/fooooo-fooooo.{fooooo,fooooo,fooooo}`` -- fooooo fooooo fooooo fooooo
    fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo, fooooo
    fooooo fooooo fooooo fooooo, fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo
    fooooo.
  * ``fooooo/fooooo.fooooo-fooooo`` -- fooooo fooooo fooooo ``fooooo-fooooo-fooooo`` fooooo fooooo
    fooooo fooooo. fooooo fooooo fooooo fooooo fooooo fooooo.

fooooo fooooo fooooo fooooo, fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo.
fooooo'fooooo fooooo fooooo fooooo fooooo fooooo fooooo ``fooooo/`` (fooooo fooooo fooooo fooooo
fooooo fooooo fooooo ``fooooo/fooooo.fooooo``) fooooo fooooo fooooo fooooo ``fooooo-fooooo``
fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo.

fooooo fooooo ``fooooo-fooooo-fooooo``
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo, fooooo.fooooo.,
``fooooo-fooooo-fooooo``. fooooo fooooo fooooo fooooo fooooo fooooo fooooo-fooooo fooooo.
fooooo'fooooo fooooo fooooo fooooo fooooo fooooo ``fooooo/fooooo.fooooo-fooooo`` fooooo, fooooo fooooo
fooooo fooooo-fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo, fooooo fooooo
fooooo fooooo ``fooooo-fooooo-fooooo`` fooooo ``fooooo-fooooo-fooooo --fooooo-fooooo=fooooo``
fooooo:

.. fooooo:: fooooo

    fooooo-fooooo:
        fooooo-fooooo-fooooo $(fooooo) --fooooo-fooooo

    fooooo-fooooo:
        fooooo-fooooo-fooooo $(fooooo) \
            --fooooo-fooooo "-fooooo -fooooo -fooooo" \
            --fooooo-fooooo

fooooo ``--fooooo-fooooo`` fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo ``fooooo``
fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo
fooooo ``fooooo-fooooo`` fooooo. fooooo ``fooooo`` fooooo, fooooo fooooo fooooo fooooo.

fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

fooooo fooooo fooooo fooooo ``fooooo`` fooooo fooooo fooooo fooooo fooooo fooooo fooooo
fooooo fooooo fooooo `fooooo <fooooo://fooooo.fooooo.fooooo/fooooo/fooooo(fooooo)>` fooooo
fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo.
fooooo fooooo fooooo fooooo fooooo ``fooooo.fooooo (fooooo)`` fooooo ``fooooo/fooooo/fooooo``.

fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo
fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo, fooooo fooooo fooooo fooooo fooooo
fooooo fooooo fooooo fooooo/fooooo/fooooo fooooo. fooooo fooooo ``.fooooo`` fooooo
fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo ``fooooo.fooooo.fooooo-fooooo``, fooooo fooooo
fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo ``fooooo.fooooo.fooooo`` fooooo fooooo fooooo
fooooo, fooooo fooooo fooooo ``fooooo.fooooo.fooooo.fooooo.fooooo.fooooo``. fooooo fooooo fooooo
fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo. fooooo fooooo fooooo fooooo
fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo (fooooo fooooo fooooo-fooooo
fooooo) fooooo. fooooo, fooooo fooooo fooooo fooooo fooooo fooooo fooooo
fooooo fooooo fooooo fooooo, fooooo fooooo fooooo fooooo ``fooooo-fooooo`` fooooo fooooo fooooo fooooo
fooooo fooooo fooooo fooooo fooooo fooooo, fooooo fooooo ``fooooo-fooooo`` fooooo, fooooo fooooo fooooo
fooooo fooooo fooooo fooooo fooooo fooooo. ``fooooo-fooooo-fooooo`` fooooo fooooo-fooooo
fooooo fooooo fooooo fooooo fooooo'fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo
``--fooooo-fooooo`` fooooo fooooo fooooo ``fooooo-fooooo`` fooooo.

fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo
fooooo fooooo fooooo fooooo fooooo/fooooo fooooo fooooo fooooo fooooo fooooo fooooo
fooooo fooooo (fooooo fooooo fooooo fooooo fooooo "fooooo" fooooo fooooo fooooo fooooo). fooooo
fooooo fooooo fooooo fooooo fooooo fooooo ``fooooo/`` fooooo fooooo fooooo fooooo fooooo
``fooooo`` fooooo fooooo fooooo fooooo fooooo fooooo fooooo ``fooooo/fooooo``. fooooo fooooo,
``fooooo-fooooo`` fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo
fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo
fooooo fooooo, fooooo fooooo fooooo fooooo.

fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo
fooooo fooooo fooooo fooooo fooooo fooooo (``fooooo fooooo ..``) fooooo'fooooo fooooo fooooo fooooo
fooooo'fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo (fooooo fooooo/fooooo fooooo
fooooo). fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo
fooooo fooooo fooooo fooooo fooooo, fooooo fooooo fooooo fooooo fooooo:

* fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo'fooooo fooooo fooooo'fooooo fooooo fooooo fooooo fooooo
  fooooo fooooo ``fooooo/fooooo`` fooooo fooooo fooooo fooooo fooooo fooooo fooooo. fooooo
  fooooo fooooo fooooo fooooo fooooo fooooo://fooooo.fooooo.fooooo/fooooo/+fooooo/fooooo,
  fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo ``fooooo/####``.
* fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo
  fooooo fooooo. fooooo fooooo fooooo fooooo fooooo fooooo.
* fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo ``fooooo/fooooo``,
  fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo
  fooooo fooooo fooooo fooooo. fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo
  ``fooooo-fooooo-fooooo-fooooo`` fooooo fooooo ``fooooo-fooooo``.

fooooo: fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo `fooooo/`
fooooo. fooooo fooooo fooooo fooooo fooooo fooooo fooooo, fooooo-fooooo fooooo fooooo
fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo-fooooo. ``fooooo-fooooo-fooooo-fooooo``
fooooo fooooo fooooo fooooo fooooo fooooo.

fooooo fooooo fooooo fooooo `fooooo fooooo fooooo fooooo fooooo fooooo` fooooo fooooo
`fooooo fooooo fooooo fooooo fooooo` fooooo fooooo fooooo fooooo fooooo fooooo fooooo
fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo-fooooo fooooo fooooo fooooo fooooo.

fooooo fooooo fooooo fooooo fooooo fooooo
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo
fooooo:

  * fooooo fooooo fooooo fooooo. fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo
    ``fooooo:`` fooooo fooooo fooooo fooooo fooooo fooooo.
  * fooooo fooooo fooooo fooooo fooooo fooooo ``fooooo`` fooooo, fooooo fooooo fooooo fooooo
    fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo'fooooo fooooo
    fooooo fooooo fooooo ``fooooo/fooooo`` fooooo. fooooo fooooo fooooo, fooooo
    ``fooooo-fooooo-fooooo-fooooo`` fooooo ``fooooo-fooooo`` fooooo fooooo fooooo
    fooooo fooooo: fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo
    fooooo fooooo fooooo fooooo fooooo fooooo, fooooo fooooo fooooo fooooo fooooo fooooo
    fooooo. fooooo fooooo fooooo fooooo fooooo fooooo, fooooo fooooo `fooooo
    fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo` fooooo
  * fooooo fooooo fooooo fooooo fooooo fooooo fooooo ``fooooo/fooooo``; fooooo
    fooooo fooooo fooooo fooooo fooooo ``fooooo-fooooo-fooooo`` fooooo fooooo
    ``fooooo-fooooo``.
  * fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo
    fooooo fooooo ``fooooo-fooooo-fooooo``.

fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo::

  * (fooooo, fooooo: fooooo/fooooo.fooooo.fooooo-fooooo.fooooo) fooooo: fooooo fooooo.fooooo.fooooo-fooooo.fooooo
  * fooooo: fooooo: fooooo fooooo fooooo fooooo fooooo fooooo
  * fooooo: fooooo fooooo
  * (fooooo/fooooo, fooooo: fooooo/fooooo.fooooo.fooooo-fooooo.fooooo) fooooo: fooooo fooooo.fooooo.fooooo-fooooo.fooooo
  * fooooo: fooooo: fooooo fooooo fooooo fooooo fooooo fooooo
  * fooooo: fooooo fooooo
  * fooooo: fooooo fooooo
  * (fooooo: fooooo/fooooo.fooooo.fooooo-fooooo.fooooo, fooooo/fooooo/fooooo/fooooo-fooooo) fooooo fooooo-fooooo fooooo fooooo.fooooo.fooooo-fooooo.fooooo fooooo fooooo/fooooo/fooooo-fooooo
  * ...

fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo-fooooo fooooo. fooooo fooooo
fooooo fooooo-fooooo fooooo, fooooo fooooo fooooo fooooo fooooo fooooo fooooo (``fooooo.fooooo.fooooo.fooooo``,
``fooooo.fooooo.fooooo.fooooo``, ``fooooo.fooooo``, fooooo.). fooooo, fooooo fooooo fooooo fooooo fooooo
fooooo fooooo fooooo fooooo fooooo-fooooo fooooo fooooo fooooo fooooo fooooo fooooo
fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo. fooooo, fooooo fooooo
fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo-fooooo.

fooooo, fooooo fooooo fooooo-fooooo fooooo fooooo fooooo fooooo fooooo ``fooooo-fooooo``,
fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo
fooooo. fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo.

fooooo fooooo fooooo ``--fooooo-fooooo`` fooooo fooooo fooooo fooooo fooooo fooooo fooooo
fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo.

fooooo fooooo fooooo fooooo fooooo
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

fooooo fooooo fooooo fooooo fooooo ``fooooo`` fooooo fooooo ``fooooo.fooooo.fooooo-fooooo`` (fooooo
fooooo: ``fooooo.fooooo.fooooo-fooooo``) fooooo fooooo fooooo ``fooooo.fooooo.fooooo-fooooo``, fooooo
fooooo fooooo fooooo fooooo.

fooooo. fooooo fooooo fooooo fooooo fooooo fooooo::

     fooooo fooooo -fooooo fooooo-fooooo-fooooo-fooooo-fooooo.fooooo.fooooo fooooo

fooooo. fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo::

     fooooo fooooo -fooooo --fooooo <fooooo fooooo> <fooooo fooooo>

   fooooo.fooooo.::

     fooooo fooooo -fooooo --fooooo fooooo/fooooo.fooooo.fooooo-fooooo fooooo/fooooo.fooooo.fooooo-fooooo

   fooooo fooooo fooooo fooooo fooooo fooooo (``fooooo: fooooo fooooo``) fooooo
   fooooo fooooo fooooo fooooo-fooooo fooooo fooooo fooooo (``fooooo: fooooo:
   fooooo``) fooooo fooooo fooooo fooooo fooooo - fooooo fooooo fooooo fooooo fooooo fooooo.

fooooo. fooooo fooooo fooooo fooooo fooooo ``fooooo`` fooooo fooooo, fooooo-fooooo fooooo fooooo
   fooooo fooooo fooooo fooooo fooooo. fooooo fooooo fooooo, fooooo fooooo
   ``fooooo-fooooo-fooooo-fooooo`` fooooo fooooo fooooo fooooo fooooo `fooooo
   fooooo fooooo fooooo fooooo fooooo` fooooo

fooooo. fooooo ``fooooo-fooooo-fooooo``, fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo
   fooooo fooooo `fooooo fooooo fooooo fooooo fooooo fooooo` fooooo.

fooooo. fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo, fooooo fooooo fooooo fooooo fooooo fooooo
   fooooo fooooo fooooo fooooo fooooo. fooooo fooooo fooooo fooooo fooooo fooooo fooooo
   `fooooo fooooo fooooo` fooooo, fooooo fooooo fooooo fooooo fooooo fooooo fooooo fooooo
   fooooo fooooo::

	 fooooo fooooo fooooo
	 fooooo fooooo --fooooo fooooo-fooooo-fooooo-fooooo.fooooo.fooooo
	 fooooo fooooo --fooooo --fooooo-fooooo fooooo fooooo

fooooo fooooo fooooo
===================

fooooo fooooo fooooo ``fooooo/fooooo.fooooo`` fooooo fooooo ``fooooo-fooooo`` fooooo
fooooo.
